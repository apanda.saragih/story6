from django.test import TestCase, Client
from django.urls import resolve
from story6app.views import home
from django.http import HttpRequest
from story6app.models import PostModel
from story6app.forms import PostForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
# Create your tests here.

class UnitTestStory6(TestCase):

	#Test views & urls
	def test_homepage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/')
		self.assertEqual(response.func, home)

	def test_homepage_using_homepage_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story6app/primary.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = home(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)

	def test_landing_page_title_is_right(self):
		request = HttpRequest()
		response = home(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>Selamat Datang!</title>', html_response)


	#SetUp and Models
	def setUp(cls):
		PostModel.objects.create(status="coba-coba")

	def test_if_models_in_database(self):
		modelsObject = PostModel.objects.create(status="Duh pusing")
		count_Object = PostModel.objects.all().count()
		self.assertEqual(count_Object, 2)

	def test_if_StatusDB_status_is_exist(self):
		modelsObject = PostModel.objects.get(id=1)
		statusObj = PostModel._meta.get_field('status').verbose_name
		self.assertEqual(statusObj, 'status')

	#Forms
	def test_forms_input_html(self):
		form = PostForm()
		self.assertIn('id="id_status', form.as_p())

	def test_forms_validation_blank(self):
		form = PostForm(data={'status':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['status'], ["This field is required."])

	#forms in templates
	def test_forms_in_template(self):
		request = HttpRequest()
		response = home(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<form method="POST" class="register_form">', html_response)



class FunctionalTestStory6(TestCase):

    def setUp(self):
    	chrome_options = webdriver.ChromeOptions()
    	chrome_options.add_argument('--dns-prefetch-disable')
    	chrome_options.add_argument('--no-sandbox')
    	chrome_options.add_argument('--headless')
    	chrome_options.add_argument('disable-gpu')
    	self.selenium = webdriver.Chrome(chrome_options=chrome_options)
    	super(FunctionalTestStory6, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory6, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('status')
        submit = selenium.find_element_by_id('button')

        status.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn("Coba Coba", selenium.page_source)
