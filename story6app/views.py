from django.shortcuts import render, redirect
from django import forms
from .forms import PostForm
from .models import PostModel
# Create your views here.


def home(request):
    post_model=PostModel.objects.all().order_by('-date')
    post_form = PostForm(request.POST or None) 
    error = None
    if post_form.is_valid():
        if request.method == 'POST':
            PostModel.objects.create(
                status  = post_form.cleaned_data.get('status'),
                )
            return redirect('story6app:home')
        else:
            error = post_form.errors
    context = {
        'isian':post_form,
        'postingan': post_model,
    }
    return render(request, 'story6app/primary.html', context)