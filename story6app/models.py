from django.db import models
from datetime import datetime
from django.core.validators import MaxLengthValidator

# Create your models here.
class PostModel(models.Model):
    status  = models.TextField(max_length=300)
    date    = models.DateTimeField(default=datetime.now, blank=True)
    